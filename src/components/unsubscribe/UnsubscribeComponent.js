import React, { Component } from 'react'
import ErrorUtil from '../common/ErrorUtil'
import { UNSUBSCRIBE_FROM_GROUP_URL } from '../../constants/BackendURLs'
import Axios from 'axios'
import { Container, Card, CardText, CardTitle, CardBody, Alert, Spinner, Row, Col } from 'reactstrap';
import { HEADER_EMAIL_KEY, HEADER_EMAIL_VALUE } from '../../constants/Constants'

export class UnsubscribeComponent extends Component {
    state = {
        data: [],
        errors: []
    }

    getAxiosUrl = (hashData) => {
        let fullUrl = UNSUBSCRIBE_FROM_GROUP_URL.concat(hashData).concat("/");
        return fullUrl;
    }

    componentDidMount() {
        let headers = {
            'user_email': HEADER_EMAIL_VALUE
        }
        const hashValue = this.props.hash
        Axios.get(this.getAxiosUrl(hashValue), { headers: headers })
            .then(res => {
                const result = res
                if (res.data.data) {
                    this.setState({ data: res.data.data });
                } else {
                    this.setState({ errors: res.data.errors });
                }
            }
            )
    }

    prepareStyledErrorResponse = (errors, text) => {
        return (
            <React.Fragment>

                <Container fluid={true} className="col-md-6 col-md-offset-3">
                    <Card>
                        <CardBody>
                            <CardTitle tag="h3">{text}</CardTitle>
                            <CardText> <Alert color="danger">{ErrorUtil.extractErrorMessage(errors)}</Alert></CardText>
                        </CardBody>
                    </Card>
                </Container>

            </React.Fragment>
        )

    }

    prepareStyledDataResponse = (data, text) => {
        return (
            <React.Fragment>
                <Container fluid={true} className="col-md-6 col-md-offset-3">
                    <Card>
                        <CardBody>
                            <CardTitle tag="h3">{data.groupName}</CardTitle>
                            <CardText> <Alert color="success">{data.userEmail + " "+ text+" for "+data.subscriptionChannel +" notification."} </Alert></CardText>
                        </CardBody>
                    </Card>
                </Container>
            </React.Fragment>
        )

    }

    render() {

        if (this.state.errors.length !== 0) {
            return this.prepareStyledErrorResponse(this.state.errors, "Unsubscription Status: Failed")

        } else if (this.state.data.length !== 0) {
            return this.prepareStyledDataResponse(this.state.data, "has been successfully unsubscribed")
        } else {
            return <div> <Container fluid={true} className="col-md-6 col-md-offset-3 mt-5">
                <Row>
                    <Col sm={{ size: 'auto', offset: 3 }}>
                        <Spinner type="grow" color="primary" />
                        <Spinner type="grow" color="secondary" />
                        <Spinner type="grow" color="success" />
                        <Spinner type="grow" color="danger" />
                        <Spinner type="grow" color="warning" />
                        <Spinner type="grow" color="info" />
                        <Spinner type="grow" color="light" />
                        <Spinner type="grow" color="dark" />
                    </Col>
                </Row>
            </Container>
            </div>
        }


    }
}

export default UnsubscribeComponent
