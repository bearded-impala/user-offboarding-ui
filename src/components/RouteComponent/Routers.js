import React, { Component } from 'react'
import UnsubscribeComponent from '../unsubscribe/UnsubscribeComponent';
import ApplicationUnsubscribeComponent from '../application-unsubcribe/ApplicationUnsubscribeComponent';
import OptOutComponent from '../OptOut/OptOutComponent';
import UpdatePreferences from '../update-preferences/UpdatePreferences';
import { UNSUBSCRIBE, APPLICATION_UNSUBSCRIBE, OPT_OUT, MANAGE_SUBSCRIPTIONS, EQUALS, AMPERSAND } from '../../constants/Constants'
import LinkRedirectionPage from '../update-preferences/LinkRedirectionPage';

const queryString = require('query-string');
export class Routers extends Component {
    parseQueryParams = (paramString) => {
        const parsed = queryString.parse(paramString);
        const stringified = queryString.stringify(parsed);
        return stringified;
    }

    getServiceName = (paramString) => {
        var str = paramString.split(AMPERSAND)
        var serviceName = str[1].split(EQUALS);
        var serviceNameSplit = serviceName[1]
        return serviceNameSplit;
    }

    getHash = (paramString) => {
        var str = paramString.split(AMPERSAND)
        var hash = str[0].split(EQUALS);
        var hashSplit = hash[1]
        return hashSplit;
    }
    render() {
        var queryParams = this.parseQueryParams(location.search)
        var serviceName = this.getServiceName(queryParams)
        var hashVal = this.getHash(queryParams)

        if (serviceName === UNSUBSCRIBE) {
            return <UnsubscribeComponent hash={hashVal} />
        } else if (serviceName === APPLICATION_UNSUBSCRIBE) {
            return <ApplicationUnsubscribeComponent hash={hashVal} />
        } else if (serviceName === OPT_OUT) {
            return <OptOutComponent hash={hashVal} />
        } else if (serviceName === MANAGE_SUBSCRIPTIONS) {
            return <LinkRedirectionPage hash={hashVal} />
        } else {
            return <div>Wrong Url</div>
        }

    }
}

export default Routers
