import React, { Component } from 'react'
import ErrorUtil from '../common/ErrorUtil'
import Axios from 'axios'
import { Container, Card, CardText, CardTitle, CardBody, Alert, Table, Spinner, Row, Col, Badge } from 'reactstrap';
import { APPLICATION_UNSUBSCRIPTION_BACKEND_URL } from '../../constants/BackendURLs'

export class ApplicationUnsubscribeComponent extends Component {
    state = {
        url: window.location.href,
        data: [],
        errors: []
    }

    getAxiosUrl = (hashData) => {
        let fullUrl = APPLICATION_UNSUBSCRIPTION_BACKEND_URL.concat(hashData).concat("/");
        return fullUrl;
    }

    componentDidMount() {
        let headers = {
            'user_email': 'asinha161@gmail.com'
        }

        const hashValue = this.props.hash
        Axios.get(this.getAxiosUrl(hashValue), { headers: headers })
            .then(res => {
                if (res.data.data) {
                    this.setState({ data: res.data.data });
                } else {
                    this.setState({ errors: res.data.errors });
                }
            }
            )
    }

    getDetailedGroupMembershipStatus = (data) => {
        return data.detailedGroupMembershipStatus.map(e => <React.Fragment><Badge color="secondary" pill>{e.groupName + ' '}</Badge><span>  </span></React.Fragment>)
    }

    getTabularData = (data) => {
        return (
            <Table>
                <thead>
                    <tr>
                        <th>Group Name</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>{this.getDetailedGroupMembershipStatus(data)}</tbody>
            </Table>
        )
    }

    getBadgeData = (data) => {
        return <div>{this.getDetailedGroupMembershipStatus(data)}</div>
    }
    prepareStyledDataResponse = (data) => {
        return (
            <React.Fragment>
                <Container className="col-md-6 col-md-offset-3 mt-5">
                    <Card>
                        <CardBody>
                            {/* (ErrorCode: {ErrorUtil.extractErrorCode(this.state.errors)}) */}
                            <CardTitle tag="h3">Application Unsubscription</CardTitle>
                            <CardText>
                                <Alert color="primary">
                                    Application Name: {data.applicationName}<br />
                                    UserName: {data.userName} <br />
                                    Status: {data.applicationUnsubscriptionStatus}<br />
                                </Alert><br />
                                <Card>
                                    <CardTitle><strong>{" "} Groups you have unsubscribed from</strong></CardTitle>
                                    <CardBody>{this.getBadgeData(data)}</CardBody>
                                </Card>
                            </CardText>
                        </CardBody>
                    </Card>
                </Container>

            </React.Fragment>
        )
    }

    prepareStyledErrorResponse = (errors, errMsg) => {
        return (
            <React.Fragment>

                <Container className="col-md-6 col-md-offset-3 mt-5">
                    <Card>
                        <CardBody>
                            {/* (ErrorCode: {ErrorUtil.extractErrorCode(this.state.errors)}) */}
                            <CardTitle tag="h3">{errMsg}</CardTitle>
                            <CardText> <Alert color="danger">{ErrorUtil.extractErrorMessage(errors)}</Alert></CardText>
                        </CardBody>
                    </Card>
                </Container>

            </React.Fragment>
        )
    }

    render() {
        if (this.state.errors.length !== 0) {
            return this.prepareStyledErrorResponse(this.state.errors, "Application Unsubscription Status: Failed")
        } else if (this.state.data.length !== 0) {
            return this.prepareStyledDataResponse(this.state.data)
        } else {
            return <div> <Container fluid={true} className="col-md-6 col-md-offset-3 mt-5">
                <Row>
                    <Col sm={{ size: 'auto', offset: 3 }}>
                        <Spinner type="grow" color="primary" />
                        <Spinner type="grow" color="secondary" />
                        <Spinner type="grow" color="success" />
                        <Spinner type="grow" color="danger" />
                        <Spinner type="grow" color="warning" />
                        <Spinner type="grow" color="info" />
                        <Spinner type="grow" color="light" />
                        <Spinner type="grow" color="dark" />
                    </Col>
                </Row>
            </Container>
            </div>
        }
    }
}



export default ApplicationUnsubscribeComponent
