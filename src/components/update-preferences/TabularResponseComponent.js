import React, { Component } from 'react'
import { Alert } from 'reactstrap'

export class TabularResponseComponent extends Component {
    renderTable = (groupId, status) => {
        return (
            <tr><td><Alert color="success">{groupId}</Alert></td><td>{status}</td></tr>
        )
    }
    render() {
        return this.renderTable(this.props.groupName, this.props.status)
    }
}

export default TabularResponseComponent

