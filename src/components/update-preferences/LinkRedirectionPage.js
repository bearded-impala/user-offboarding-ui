import React, { Component } from 'react'
import { Alert, Spinner, Row, Col, Form, FormGroup, Container, CustomInput, Card, CardText, CardHeader, Button } from 'reactstrap';
import BackendRequestRedirector from './BackendRequestRedirector';
import UpdateUserPreferences from './update-user-preferences/UpdatePreferences';

export class LinkRedirectionPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hashValue: "",
            manageSubscriptionButtonClicked: false,
            updatePreferencesButtonClicked: false
        }

        this.handleManageClick = this.handleManageClick.bind(this);
        this.handleUpdatePreference = this.handleUpdatePreference.bind(this);
    }

    componentDidMount() {
        this.setState({ hashValue: this.props.hash })
    }

    handleManageClick() {
        console.log("handleClick called")
        console.log("hashValue " + this.state.hashValue)
        this.setState({ manageSubscriptionButtonClicked: true })
    }

    handleUpdatePreference() {
        console.log("Update Preferences called")
        console.log("hashValue " + this.state.hashValue)
        this.setState({ updatePreferencesButtonClicked: true })
    }

    buttonInterface = () => {
        return (
            <Container>


                <Card>
                    <CardHeader tag="h3">Where do you want to go ?</CardHeader>
                    <br />
                    <FormGroup check>
                        <ul>
                            <li><Button color="link" onClick={this.handleManageClick}>Manage Subscriptions</Button></li>
                            <li><Button color="link" onClick={this.handleUpdatePreference}>Update User Preferences</Button></li>
                        </ul>
                    </FormGroup>
                </Card>
            </Container>
        )
    }
    render() {
        if (this.state.manageSubscriptionButtonClicked) {
            return <BackendRequestRedirector hashParam={this.state.hashValue} />
        } else if (this.state.updatePreferencesButtonClicked) {
            return <UpdateUserPreferences />
        } else {
            return (
                //<Button color="link" onClick={this.handleManageClick}>Manage Subscriptions</Button>
                this.buttonInterface()
            )
        }
    }
}


export default LinkRedirectionPage
