import React, { Component } from 'react'
import { Input, Button, Form, FormGroup, Label, FormText, Container, Row, Col } from 'reactstrap';

export class UpdateUserNotificationPreferences extends Component {

    prepareCheckboxRepresentation = () => {
        return (
            <Container>

                <Form>
                    <FormGroup check>
                        <Label check>
                            <Input type="checkbox" />{' '}
                            SMS
                    </Label>
                        <br />
                        <Label check>
                            <Input type="checkbox" />{' '}
                            E-MAIL
                    </Label>
                    </FormGroup>
                    <Button className='mt-3 mb-3 mr-3'>Submit</Button>
                </Form>
            </Container>
        )
    }
    render() {
        return (
            <div>
                {this.prepareCheckboxRepresentation()}
            </div>
        )
    }
}

export default UpdateUserNotificationPreferences
