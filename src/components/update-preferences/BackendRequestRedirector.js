import React, { Component } from 'react'
import Axios from 'axios'
import OutputComponent from '../update-preferences/OutputComponent';
import { Alert, Spinner, Row, Col, Form, FormGroup, Container, CustomInput, Card, CardText, CardHeader, Button } from 'reactstrap';
import { GET_APPLICATION_SEGREGATED_ASSOCIATION_OF_USER_GROUP_MAPPING, URL_FOR_REMOVING_USER_GROUP_ASSOCIATION_IN_BULK } from '../../constants/BackendURLs'
import { BrowserRouter, Route, Link, Switch } from "react-router-dom";

export class BackendRequestRedirector extends Component {
    constructor(props) {
        super(props)
        this.state = {
            persons: [],
            errors: [],
            checkedGroupIds: '',
            unsubscriptionResponse: []
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    getAxiosUrl = (hashData) => {
        let fullUrl = GET_APPLICATION_SEGREGATED_ASSOCIATION_OF_USER_GROUP_MAPPING.concat(hashData).concat("/");
        console.log(fullUrl)
        return fullUrl;
    }

    componentDidMount() {
        let headers = {
            'user_email': 'asinha161@gmail.com'
        }
        Axios.get(this.getAxiosUrl(this.props.hashParam), { headers: headers })
            .then(res => {
                const persons = res.data.data;
                if (res.data.data) {
                    const persons = res.data.data;
                    this.setState({ persons });
                } else {
                    const error = res.data.errors
                    console.log(error)
                    this.setState({ errors: error })
                }
            })
    }

    getEmail = (persons) => {
        return persons.userEmail
    }


    getUserId = (persons) => {
        return persons.userId
    }


    getUserName = (persons) => {
        return persons.userName
    }


    getGroupApplicationModelData = (person) => {
        let groupApplicationModelData = []
        if (person.groupApplicationModel) {
            groupApplicationModelData = person.groupApplicationModel
        }
        return groupApplicationModelData
    }

    getGroupIds = (person) => {
        let groupApplicationModelData = this.getGroupApplicationModelData(person)
        let groupIds = groupApplicationModelData.map(x => x.groupId)
        return groupIds
    }

    getGroupNames = (person) => {
        let groupApplicationModelData = this.getGroupApplicationModelData(person)
        let groupNames = groupApplicationModelData.map(x => x.groupName)
        return groupNames
    }

    getApplicationNames = (person) => {
        let groupApplicationModelData = this.getGroupApplicationModelData(person)
        let applicationName = groupApplicationModelData.map(x => x.applicationDetail.applicationName)
        return applicationName
    }

    getApplicationIds = (person) => {
        let groupApplicationModelData = this.getGroupApplicationModelData(person)
        let applicationIds = groupApplicationModelData.map(x => x.applicationDetail.applicationId)
        return applicationIds
    }

    handleChange(event) {
        if (event.target.checked) {
            //event.target.removeAttribute('checked');
            console.log("this called: event.target.checked")
            let id = event.target.id
            this.setState((prevState) => ({ checkedGroupIds: prevState.checkedGroupIds.concat(",").concat(id) }))
        } else {
            console.log("this called: event.target.checked=false")

            let id = event.target.id
            this.setState((prevState) => ({ checkedGroupIds: prevState.checkedGroupIds.replace("," + id, "") }))
        }
    }


    renderCheckbox = (id, name) => {
        return <CustomInput type="checkbox" id={id} name={id} value={name} onChange={this.handleChange} label={name} />
    }

    renderCheckbox_2 = (id, name) => {
        return <React.Fragment><label><input type="checkbox" id={id} name={id} value={name} onChange={this.handleChange} checked={true} />{name}</label> <br /></React.Fragment>
    }
    findGroupsPerApplication = (applicationName, person) => {
        let groupApplicationModelData = this.getGroupApplicationModelData(person)
        let applicationList = []
        groupApplicationModelData.forEach(x => {
            if (applicationName === x.applicationDetail.applicationName) {
                let checkbox = this.renderCheckbox(x.groupId, x.groupName)

                applicationList.push(checkbox)
            }
        })
        return <div>{applicationList}</div>
    }


    distinctApplicationView = (person) => {
        let groupApplicationModelData = this.getGroupApplicationModelData(person)
        let appSet = new Set()
        groupApplicationModelData.map(x => appSet.add(x.applicationDetail.applicationName))
        return appSet
    }


    organizeView = (person) => {
        let appSet = this.distinctApplicationView(person)
        let appSetElems = []
        appSet.forEach(applicationName => {
            appSetElems.push(<div align="left"><strong>Application: </strong>{applicationName}<br />{this.findGroupsPerApplication(applicationName, person)}</div>)
            appSetElems.push(<br />)

        })

        return <div>{appSetElems}</div>
    }



    differenceOf2Arrays = (array1, array2) => {
        var temp = [];
        array1 = array1.toString().split(',').map(Number);
        array2 = array2.toString().split(',').map(Number);

        for (var i in array1) {
            if (array2.indexOf(array1[i]) === -1) temp.push(array1[i]);
        }
        for (i in array2) {
            if (array1.indexOf(array2[i]) === -1) temp.push(array2[i]);
        }
        return temp.sort((a, b) => a - b);
    }

    reverseCheckboxSelects = (selectedCheckboxesConcatString, person) => {
        let allGroupIdList = this.getGroupIds(person) //fetch array of groupId
        let clickedGroupIds = selectedCheckboxesConcatString
        let checkedGroupIdList = clickedGroupIds.split(",")
        let result = this.differenceOf2Arrays(allGroupIdList, checkedGroupIdList)
        return result.toString()
    }

    handleSubmit(event) {
        event.preventDefault();
        const axios = require('axios');
        const persons = this.state.persons
        let userId = this.getUserId(persons)
        let userEmailId = this.getEmail(persons)
        let groupIds = this.reverseCheckboxSelects(this.state.checkedGroupIds.replace(/,/, ''), persons)
        let data = JSON.stringify({
            userId: userId,
            userEmailId: userEmailId,//
            groupIds: groupIds
        })
        //console.log("reverse checkbox selected groupIds " + groupIds)
        //console.log("Post Request " + data)
        let url = URL_FOR_REMOVING_USER_GROUP_ASSOCIATION_IN_BULK
        Axios.post(url, data, {
            headers: {
                'user_email': 'asinha161@gmail.com',
                'Content-Type': 'application/json'
            }
        }).then(res => {
            console.log(res)
            const response = res.data
            console.log(response)
            if (res.status === 200) {

                this.setState({ unsubscriptionResponse: response })
                console.log(response)
            } else {
                console.log(response)
            }
        })
        console.log(data)
    }


    renderCheckboxedResponse = (person) => {
        return (
            <React.Fragment>
                <br />
                <Container>


                    <Card>
                        <CardHeader tag="h3">Manage Subscriptions for : {this.getUserName(person)}</CardHeader>
                        <br />
                        <FormGroup check>
                            <Form onSubmit={this.handleSubmit}>
                                {this.organizeView(person)}
                                <Button color="primary">Submit</Button>{' '}
                                <br />
                                <br /><br />
                            </Form>

                        </FormGroup>
                    </Card>
                </Container>
            </React.Fragment>
        )
    }

    renderErrorResponse = (error) => {
        console.log(error)
        let errorMessages = error.map(element => element.errorMessage)
        return <h1>{errorMessages}</h1>
    }

    render() {

        if (this.state.unsubscriptionResponse.length === 0) {
            if (this.state.persons.length !== 0) {
                return <Container>{this.renderCheckboxedResponse(this.state.persons)}</Container>
            } else if (this.state.errors.length !== 0) {
                return (
                    <Container>
                        <Card>
                            <CardText> <Alert color="danger">{this.renderErrorResponse(this.state.errors)}</Alert></CardText>
                        </Card>
                    </Container>
                )
            } else {
                return <div> <Container fluid={true} className="col-md-6 col-md-offset-3 mt-5">
                    <Row>
                        <Col sm={{ size: 'auto', offset: 3 }}>
                            <Spinner type="grow" color="primary" />
                            <Spinner type="grow" color="secondary" />
                            <Spinner type="grow" color="success" />
                            <Spinner type="grow" color="danger" />
                            <Spinner type="grow" color="warning" />
                            <Spinner type="grow" color="info" />
                            <Spinner type="grow" color="light" />
                            <Spinner type="grow" color="dark" />
                        </Col>
                    </Row>
                </Container>
                </div>
            }

        } else {
            return (
                <Container>{<OutputComponent stateData={this.state.unsubscriptionResponse} persons={this.state.persons} />}</Container>

            )
        }



    }
}

export default BackendRequestRedirector
